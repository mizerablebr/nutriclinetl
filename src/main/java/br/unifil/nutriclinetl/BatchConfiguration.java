package br.unifil.nutriclinetl;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.ItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.sql.Types;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    public static final String PATIENTOLD_QUERY = "SELECT nome, endereco, cidadeEstado, dataNascimento, estadoCivil, " +
            "telefone, telefone2, nroFilhos, ocupacao FROM clinica.cadastro_paciente;";
    public static final String PATIENT_QUERY = "INSERT INTO nutriclin.patient" +
            "(name, address_street, address_number, address_city, address_state, born_date, marital_status, phone_number1," +
            "phone_number2, children_number, occupation, active) VALUES (:name, :addressStreet, :addressNumber, :addressCity, :addressState," +
            ":bornDate, :maritalStatus, :phoneNumber1, :phoneNumber2, :childrenNumber, :occupation, :active);";

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public ItemReader<PatientOld> itemReader(@Qualifier("incomingDataSource") DataSource dataSource) {
        return new JdbcCursorItemReaderBuilder<PatientOld>()
                .name("PatienteReader")
                .dataSource(dataSource)
                .sql(PATIENTOLD_QUERY)
                .rowMapper(new BeanPropertyRowMapper<>(PatientOld.class))
                .build();
    }

    @Bean
    public PatientProcessor processor() {
        return new PatientProcessor();
    }

    /*@Bean
    public FlatFileItemWriter<Patient> writer() {
        FlatFileItemWriter<Patient> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource("output/output.csv"));
        writer.setAppendAllowed(true);
        writer.setLineAggregator(new DelimitedLineAggregator<Patient>() {
            {
                setDelimiter(",");
                setFieldExtractor(new BeanWrapperFieldExtractor<Patient>() {
                    {
                        setNames(new String[] { "name", "bornDate", "addressStreet" });
                    }
                });
            }
        });
        return writer;
    }*/

    @Bean
    public ItemWriter<Patient> itemWriter(@Qualifier("outgoingDataSource") DataSource dataSourceOut) {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSourceOut);
        JdbcBatchItemWriter<Patient> databaseItemWriter = new JdbcBatchItemWriter<>();
        databaseItemWriter.setDataSource(dataSourceOut);
        databaseItemWriter.setJdbcTemplate(jdbcTemplate);
        databaseItemWriter.setSql(PATIENT_QUERY);
        ItemSqlParameterSourceProvider<Patient> paramProvider = new BeanPropertyItemSqlParameterSourceProvider<>() {
            @Override
            public SqlParameterSource createSqlParameterSource(Patient item) {
                return new BeanPropertySqlParameterSource(item) {
                    @Override
                    public Object getValue(String paramName) throws IllegalArgumentException {
                        if (paramName.equalsIgnoreCase("maritalStatus"))
                            return ((Patient.MaritalStatus) super.getValue(paramName)).ordinal();
                        return super.getValue(paramName);
                    }
                };
            }
        };
        databaseItemWriter.setItemSqlParameterSourceProvider(paramProvider);
        return databaseItemWriter;
    }

    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importPatientJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(ItemWriter<Patient> writer, ItemReader<PatientOld> reader) {
        return stepBuilderFactory.get("step1")
                .<PatientOld, Patient> chunk(10)
                .reader(reader)
                .processor(processor())
                .writer(writer)
                .build();
    }
}
