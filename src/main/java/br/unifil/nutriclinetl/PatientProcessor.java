package br.unifil.nutriclinetl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatientProcessor implements ItemProcessor<PatientOld, Patient> {
    private static final Logger logger = LoggerFactory.getLogger(PatientProcessor.class);
    @Override
    public Patient process(PatientOld patientOld) throws Exception {
        Patient patient = new Patient();
        patient.setActive(true);
        patient.setName(patientOld.getNome().trim());
        patient.setAddressStreet(processAddress(patientOld.getEndereco()));
        patient.setAddressNumber(processAddressNumber(patientOld.getEndereco()));
        String[] cityState = patientOld.getCidadeEstado().split("/");
        if (cityState.length > 1) {
            patient.setAddressCity(cityState[0]);
            patient.setAddressState(cityState[1]);
        }
        patient.setBornDate(processBornDate(patientOld.getDataNascimento()));
        patient.setMaritalStatus(processMaritalStatus(patientOld.getEstadoCivil()));
        patient.setPhoneNumber1(patientOld.getTelefone());
        patient.setPhoneNumber2(patientOld.getTelefone2());
        patient.setChildrenNumber(Integer.valueOf(patientOld.getNroFilhos()).shortValue());
        patient.setOccupation(patientOld.getOcupacao());
        //logger.info("Converting {} into {}", patientOld, patient);
        return patient;
    }
    private String processAddress(String oldAddress) {
        // Retorna porção do endereço antes do primeiro número
        Pattern p = Pattern.compile("([a-z]|[A-z]|\\s|,|\\.|:)*(?=\\ \\d)");
        Matcher m = p.matcher(oldAddress);
        if (m.find())
            return m.group(0);
        else
            return oldAddress;
    }

    private long processAddressNumber(String oldAdress) {
        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(oldAdress);
        if (m.find())
            try {
                return Long.parseLong(m.group(0));
            } catch (NumberFormatException e) {
                return 0;
            }
        else
            return 0;
    }

    private LocalDate processBornDate(String oldDate) {
        try {
            return LocalDate.parse(oldDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    private Patient.MaritalStatus processMaritalStatus(String maritalStatus) {
        maritalStatus = maritalStatus.toUpperCase();
        if (maritalStatus.contains("Ú"))
            maritalStatus = maritalStatus.replace("Ú", "U");
        if (maritalStatus.equals("SEPARADO"))
            maritalStatus = "DIVORCIADO";
        try {
            return Patient.MaritalStatus.valueOf(maritalStatus);
        } catch (IllegalArgumentException e) {
            return Patient.MaritalStatus.SOLTEIRO;
        }

    }
}
