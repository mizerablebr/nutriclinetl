package br.unifil.nutriclinetl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NutriclinetlApplication {

    public static void main(String[] args) {
        System.exit(SpringApplication.exit(SpringApplication.run(NutriclinetlApplication.class, args)));
    }

}
