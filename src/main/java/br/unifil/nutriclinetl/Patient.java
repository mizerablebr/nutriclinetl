package br.unifil.nutriclinetl;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

public class Patient {

    private Long code;
    private String name;

    private String rg;
    private String cpf;
    private Gender gender;
    private String addressStreet;
    private Long addressNumber;
    private String addressCity;
    private String addressState;
    private String cep;
    private LocalDate bornDate;
    private MaritalStatus maritalStatus;
    private String phoneNumber1;
    private String phoneNumber2;
    private String phoneNumber3;
    private short childrenNumber;
    private String occupation;
    private Boolean paying;

    private Boolean active;

    public Patient() {
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public Long getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(Long addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public LocalDate getBornDate() {
        return bornDate;
    }

    public void setBornDate(LocalDate bornDate) {
        this.bornDate = bornDate;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getPhoneNumber3() {
        return phoneNumber3;
    }

    public void setPhoneNumber3(String phoneNumber3) {
        this.phoneNumber3 = phoneNumber3;
    }

    public short getChildrenNumber() {
        return childrenNumber;
    }

    public void setChildrenNumber(short childrenNumber) {
        this.childrenNumber = childrenNumber;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Boolean getPaying() {
        return paying;
    }

    public void setPaying(Boolean paying) {
        this.paying = paying;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", rg='" + rg + '\'' +
                ", cpf='" + cpf + '\'' +
                ", gender=" + gender +
                ", addressStreet='" + addressStreet + '\'' +
                ", addressNumber=" + addressNumber +
                ", addressCity='" + addressCity + '\'' +
                ", addressState='" + addressState + '\'' +
                ", cep='" + cep + '\'' +
                ", bornDate=" + bornDate +
                ", maritalStatus=" + maritalStatus +
                ", phoneNumber1='" + phoneNumber1 + '\'' +
                ", phoneNumber2='" + phoneNumber2 + '\'' +
                ", phoneNumber3='" + phoneNumber3 + '\'' +
                ", childrenNumber=" + childrenNumber +
                ", occupation='" + occupation + '\'' +
                ", paying=" + paying +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return getCode().equals(patient.getCode()) &&
                getName().equals(patient.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode(), getName());
    }

    public enum Gender {
        NAO_INFORMAR("Não informar"), FEMININO("Feminino"), MASCULINO("Masculino"), ;
        private final String displayName;

        Gender(String name) {
            this.displayName = name;
        }
        public String getDisplayName() {return  this.displayName;}
    }

    public enum MaritalStatus {
        SOLTEIRO("Solteiro(a)"), CASADO("Casado(a)"), VIUVO("Viuvo(a)"), DIVORCIADO("Divorciado(a)");
        private final String displayName;
        MaritalStatus(String name) {
            this.displayName = name;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
