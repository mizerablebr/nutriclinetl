package br.unifil.nutriclinetl;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfiguration {
    @Bean
    @Primary
    public DataSource datasource() {
        DataSourceBuilder datasource = DataSourceBuilder.create();
        datasource.url("jdbc:h2:file:./db");
        datasource.driverClassName("org.h2.Driver");
        datasource.username("sa");
        datasource.password("");
        return datasource.build();
    }

    @Bean(name = "incomingDataSource")
    public DataSource incomingDataSource() {
        DataSourceBuilder datasource = DataSourceBuilder.create();
        datasource.url("jdbc:mysql://127.0.0.1:3310/clinica");
        datasource.driverClassName("com.mysql.jdbc.Driver");
        datasource.username("root");
        datasource.password("root");
        return datasource.build();
    }

    @Bean(name = "outgoingDataSource")
    public DataSource outgoingDataSource() {
        DataSourceBuilder datasource = DataSourceBuilder.create();
        datasource.url("jdbc:mysql://127.0.0.1:3307/nutriclin");
        datasource.driverClassName("com.mysql.jdbc.Driver");
        datasource.username("root");
        datasource.password("root");
        return datasource.build();
    }
}
