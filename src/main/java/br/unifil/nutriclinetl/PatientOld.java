package br.unifil.nutriclinetl;

import lombok.Data;

@Data
public class PatientOld {
    private long cod_paciente;
    private String nome;
    private String endereco;
    private String cidadeEstado;
    private String dataNascimento;
    private String dataCadastro;
    private String telefone;
    private String telefone2;
    private String naturalidade;
    private String ocupacao;
    private String estadoCivil;
    private String idade;
    private int nroFilhos;
}
